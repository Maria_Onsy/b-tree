package eg.edu.alexu.csd.filestructure.btree;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.print.attribute.standard.MediaSize.Engineering;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SearchEngine implements ISearchEngine{
    IBTree<String,List<ISearchResult>> tree;
    public SearchEngine(int m) {
    	tree=new BTree(m);
    }
	@Override
	public void indexWebPage(String filePath) {
		if(filePath==null) {throw new RuntimeErrorException(null);}
		File temp=new File(filePath);
        if(temp.exists()) {
        	parse(temp);
        }
        else {throw new RuntimeErrorException(null);}
	}

	@Override
	public void indexDirectory(String directoryPath) {
		if(directoryPath==null) {throw new RuntimeErrorException(null);}
		File temp=new File(directoryPath);
        if(temp.exists()) {
        	File files[]=temp.listFiles();
        	if(files==null) {throw new RuntimeErrorException(null);}
    		for(int i=0;i<files.length;i++) {
    			if(files[i].isDirectory()) {indexDirectory(directoryPath+"/"+files[i].getName());}
    			else{indexWebPage(directoryPath+"/"+files[i].getName());}
    		}
        }
        else {throw new RuntimeErrorException(null);}
	}

	@Override
	public void deleteWebPage(String filePath) {
		if(filePath==null) {throw new RuntimeErrorException(null);}
		File temp=new File(filePath);
        if(temp.exists()) {
		try {
	    	DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
			DocumentBuilder db=dbf.newDocumentBuilder();
			Document doc=db.parse(temp);
			doc.getDocumentElement().normalize();
			NodeList nodeList=doc.getElementsByTagName("doc");
			for(int i=0;i<nodeList.getLength();i++) {
				Node node=nodeList.item(i);
				NamedNodeMap nodeMap=node.getAttributes();
				String id=nodeMap.getNamedItem("id").getNodeValue();
				Node tempN=node.getFirstChild();
				String sen=tempN.toString();
				sen=sen.replaceAll("\\n"," ");
				sen=sen.replaceAll("]","");
				sen=sen.replaceAll("\\s+", " ");
				String[] words=sen.split(" ");
				for(int j=0;j<words.length;j++) {
					String word=words[j].toLowerCase();
					if(word.startsWith("[")) {continue;}
					List<ISearchResult> t=tree.search(word);
					if(t==null) {continue;}
					else if(t.size()==1&&t.get(0).getId().equals(id)) {tree.delete(word);}
					else {
						for(int k=0;k<t.size();k++) {
							if(t.get(k).getId().equals(id)) {
								t.remove(t.get(k));
								break;}
						}
					}
				}
			}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
        }
        else {throw new RuntimeErrorException(null);}
	}

	@Override
	public List<ISearchResult> searchByWordWithRanking(String word) {
		if(word==null) {throw new RuntimeErrorException(null);}
		word=word.toLowerCase();
		List<ISearchResult> r=tree.search(word);
		if(r==null) {r=new ArrayList<ISearchResult>();}
		return r;
	}

	@Override
	public List<ISearchResult> searchByMultipleWordWithRanking(String sentence) {
		if(sentence==null) {throw new RuntimeErrorException(null);}
		List<ISearchResult> result=new ArrayList<ISearchResult>();
		sentence=sentence.toLowerCase();
		sentence=sentence.replaceAll("\\n"," ");
		sentence=sentence.replaceAll("\\s+"," ");
		if(sentence.startsWith(" ")) {sentence=sentence.replaceFirst(" ","");}
		String words[]=sentence.split(" ");
		ArrayList<List<ISearchResult>> res=new ArrayList<List<ISearchResult>>();
		for(int i=0;i<words.length;i++) {
			List<ISearchResult> temp=searchByWordWithRanking(words[i]);
			if(temp==null) { result=new ArrayList<ISearchResult>();
				return result;}
			res.add(temp);
		}
		ArrayList<String> ids=new ArrayList<String>();
		ArrayList<Boolean> f=new ArrayList<Boolean>();
		ArrayList<Integer> ranks=new ArrayList<Integer>();
		for(int i=0;i<res.size();i++) {
			List<ISearchResult> temp=res.get(i);
			for(int j=0;j<temp.size();j++) {
				ISearchResult t=temp.get(j);
			if(i==0) {
				ids.add(t.getId());
				ranks.add(t.getRank());
				f.add(true);
			}
			else {
				if(ids.contains(temp.get(j).getId())&&f.get(ids.indexOf(t.getId()))==true) {
					if(ranks.get(ids.indexOf(t.getId()))>t.getRank()) {
						ranks.set(ids.indexOf(t.getId()),t.getRank());
					 }
				 }
				else {f.add(false);}
			  }
		   }
		}
		for(int i=0;i<ids.size();i++) {
			if(f.get(i)==true) {
				ISearchResult t=new SearchResult(ids.get(i), ranks.get(i));
				result.add(t);
			}
		}
		return result;
	}
	
	
	public void parse(File path) {
		try {
    	DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
		DocumentBuilder db=dbf.newDocumentBuilder();
		Document doc=db.parse(path);
		doc.getDocumentElement().normalize();
		NodeList nodeList=doc.getElementsByTagName("doc");
		for(int i=0;i<nodeList.getLength();i++) {
			Node node=nodeList.item(i);
			NamedNodeMap nodeMap=node.getAttributes();
			String id=nodeMap.getNamedItem("id").getNodeValue();
			Node temp=node.getFirstChild();
			String sen=temp.toString();
			sen=sen.replaceAll("\\n"," ");
			sen=sen.replaceAll("]","");
			sen=sen.replaceAll("\\s+", " ");
			String[] words=sen.split(" ");
			for(int j=0;j<words.length;j++) {
				String word=words[j].toLowerCase();
				if(word.startsWith("[")) {continue;}
				List<ISearchResult> found=tree.search(word);
				if(found==null) {
					List<ISearchResult> results=new ArrayList<ISearchResult>();
					SearchResult result=new SearchResult(id, 1);
					results.add(result);
					tree.insert(word, results);
				}
				else {
					boolean f=false;
					for(int k=0;k<found.size();k++) {
						ISearchResult t=found.get(k);
						if(t.getId()==id) {
							t.setRank(t.getRank()+1);
							f=true;
							break;
						}
					}
					if(!f) {
						SearchResult res=new SearchResult(id, 1);
						found.add(res);
					}
				}
			}
		}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
    }
}
