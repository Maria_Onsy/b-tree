package eg.edu.alexu.csd.filestructure.btree;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

public class BTree<K extends Comparable<K>,V> implements IBTree<K, V>{
	IBTreeNode<K, V> root=new BTreeNode<K,V>();
	private int min;
	private int max;
	public BTree(int m) {
		if(m<=1) {throw new RuntimeErrorException(null);}
		else {this.min=m;
		      max=(min*2)-1;}
	}
	
	
	@Override
	public int getMinimumDegree() {
		return min;
	}

	@Override
	public IBTreeNode<K, V> getRoot() {
		if(root.getNumOfKeys()==0) {return null;}
		return root;
	}

	@Override
	public void insert(K key, V value) {
		if(key==null||value==null) {throw new RuntimeErrorException(null);}
		if(root.getNumOfKeys()==0) {
			root.getKeys().add(key);
			root.getValues().add(value);
		}
		if(search(key)!=null) {return;}
		else {insert(root, key, value);}
	}

	@Override
	public V search(K key) {
		if(key==null) {throw new RuntimeErrorException(null);}
		IBTreeNode<K, V> node=root;
		while(node!=null) {
		List<K> keys=new ArrayList<K>();
		keys.addAll(node.getKeys());
		List<V> values=new ArrayList<V>();
		values.addAll(node.getValues());
		int r=0;
		for(r=0;r<keys.size();r++) {
		  if(keys.get(r).equals(key)) {return values.get(r);}
			if(keys.get(r).compareTo(key)>0) {break;}
		}
		if(node.isLeaf()) {return null;}
		node=node.getChildren().get(r);}
		return null;
	}

	@Override
	public boolean delete(K key) {
		if(key==null) {throw new RuntimeErrorException(null);}
		if(root.getNumOfKeys()==1&&root.getKeys()==key) {
			root.setKeys(new ArrayList<K>());
			root.setValues(new ArrayList<V>());
		}
		if(search(key)==null) {return false;}
		else {return delete(root,key);}
	}
	
	public void insert(IBTreeNode<K, V> node,K key, V value) {
		List<K> keys=new ArrayList<K>();
		keys.addAll(node.getKeys());
		List<V> values=new ArrayList<V>();
		values.addAll(node.getValues());
		int r=0;
		for(r=0;r<keys.size();r++) {
		  if(keys.get(r).equals(key)) {return;}
			if(keys.get(r).compareTo(key)>0) {break;}
		}
		if(node.isLeaf()) {
		if(node.getNumOfKeys()<max) {
			keys.add(r,key);
			values.add(r,value);
			node.setKeys(keys);
			node.setValues(values);
		}
		else if(node.getNumOfKeys()==max) {
			if(node==root) {
				splitRoot(node);
				insert(node,key,value);
			}
		}
	  }
		else if(node==root&&node.getNumOfKeys()==max) {
			splitRoot(node);
			insert(node,key,value);
		}
		else {
			IBTreeNode<K, V> child=node.getChildren().get(r);
			if(child.getNumOfKeys()<max) {insert(child, key, value);}
			else if(node.getNumOfKeys()<max){
				split(node, r);
				if(node.getKeys().get(r).compareTo(key)<0) {child=node.getChildren().get(r+1);}
				insert(child, key, value);
			}
		}
		
	}
	public void splitRoot(IBTreeNode<K, V> node) {
		List<K> keys=new ArrayList<K>();
		keys.addAll(node.getKeys());
		List<V> values=new ArrayList<V>();
		values.addAll(node.getValues());
		List<IBTreeNode<K, V>> children=new ArrayList<IBTreeNode<K,V>>();
		if(!node.isLeaf()) {
		children.addAll(node.getChildren());}
		List<K> krt=new ArrayList<K>();
		List<V> vrt=new ArrayList<V>();
		krt.add(keys.get(min-1));
		vrt.add(values.get(min-1));
		node.setKeys(krt);
		node.setValues(vrt);
		IBTreeNode<K, V> Lchild=new BTreeNode<K,V>();
		List<K> kLch=new ArrayList<K>();
		List<V> vLch=new ArrayList<V>();
		List<IBTreeNode<K, V>> Lchildren=new ArrayList<IBTreeNode<K,V>>();
		for(int i=0;i<min-1;i++) {
			kLch.add(keys.get(i));
			vLch.add(values.get(i));
			if(!node.isLeaf()){
			Lchildren.add(children.get(i));}
		}
		if(!node.isLeaf()){
			Lchildren.add(children.get(min-1));}
		Lchild.setKeys(kLch);
		Lchild.setValues(vLch);
		if(!node.isLeaf()){
		Lchild.setChildren(Lchildren);
		Lchild.setLeaf(false);}
		IBTreeNode<K, V> Rchild=new BTreeNode<K,V>();
		List<K> kRch=new ArrayList<K>();
		List<V> vRch=new ArrayList<V>();
		List<IBTreeNode<K, V>> Rchildren=new ArrayList<IBTreeNode<K,V>>();
		for(int i=min;i<keys.size();i++) {
			kRch.add(keys.get(i));
			vRch.add(values.get(i));
			if(!node.isLeaf()){
				Rchildren.add(children.get(i));}
		}
		if(!node.isLeaf()){
			Rchildren.add(children.get(keys.size()));}
		Rchild.setKeys(kRch);
		Rchild.setValues(vRch);
		if(!node.isLeaf()){
		Rchild.setChildren(Rchildren);
		Rchild.setLeaf(false);}
		List<IBTreeNode<K, V>> RTchildren=new ArrayList<IBTreeNode<K,V>>();
		RTchildren.add(Lchild);
		RTchildren.add(Rchild);
		node.setChildren(RTchildren);
		node.setLeaf(false);
	}
	public void split(IBTreeNode<K, V>node,int r) {
		   IBTreeNode<K, V> child=node.getChildren().get(r);
		   List<K> keys=new ArrayList<K>();
		   keys.addAll(child.getKeys());
		   List<V> values=new ArrayList<V>();
		   values.addAll(child.getValues());
		   List<IBTreeNode<K, V>> children=new ArrayList<IBTreeNode<K,V>>();
			if(!child.isLeaf()) {
			children.addAll(child.getChildren());}
		   node.getKeys().add(r,keys.get(min-1));
		   node.getValues().add(r,values.get(min-1));
			IBTreeNode<K, V> Rchild=new BTreeNode<K,V>();
			List<K> kRch=new ArrayList<K>();
			List<V> vRch=new ArrayList<V>();
			List<IBTreeNode<K, V>> Rchildren=new ArrayList<IBTreeNode<K,V>>();
			for(int i=keys.size()-1;i>=min-1;i--) {
				child.getKeys().remove(i);
				child.getValues().remove(i);
			}
			if(!child.isLeaf()){
				for(int i=max;i>min-1;i--) {
				child.getChildren().remove(i);}
				}
			for(int i=min;i<keys.size();i++) {
				kRch.add(keys.get(i));
				vRch.add(values.get(i));
				if(!child.isLeaf()){
					Rchildren.add(children.get(i));}
			}
			if(!child.isLeaf()){
				Rchildren.add(children.get(keys.size()));}
			Rchild.setKeys(kRch);
			Rchild.setValues(vRch);
			if(!child.isLeaf()){
				Rchild.setChildren(Rchildren);
				Rchild.setLeaf(false);}
			node.getChildren().add(r+1,Rchild);
	   }
	
	public boolean delete(IBTreeNode< K, V> node,K key) {
		List<K> keys=new ArrayList<K>();
		keys.addAll(node.getKeys());
		List<V> values=new ArrayList<V>();
		values.addAll(node.getValues());
		int r=0;
		for(r=0;r<keys.size();r++) {
		  if(keys.get(r).equals(key)) {
			  if(node.isLeaf()) {
					values.remove(r);
					keys.remove(r);
					node.setKeys(keys);
					node.setValues(values);
					}
			  else {
				  IBTreeNode<K, V> left=node.getChildren().get(r);
				  IBTreeNode<K, V> right=node.getChildren().get(r+1);
				  if(left.getNumOfKeys()>min-1) {
					  IBTreeNode<K, V> maxN=left;
					  while(!maxN.isLeaf()) {
						  maxN=maxN.getChildren().get(maxN.getNumOfKeys());
					  }
					  int t=maxN.getNumOfKeys()-1;
					  keys.remove(r);
					  keys.add(r, maxN.getKeys().get(t));
					  values.remove(r);
					  values.add(r,maxN.getValues().get(t));
					  node.setKeys(keys);
					  node.setValues(values);
					  delete(left,maxN.getKeys().get(t));
					  }
				  else if(right.getNumOfKeys()>min-1) {
					  IBTreeNode<K, V> min=right;
					  while(!min.isLeaf()) {
						  min=min.getChildren().get(0);
					  }
					  keys.remove(r);
					  keys.add(r, min.getKeys().get(0));
					  values.remove(r);
					  values.add(r,min.getValues().get(0));
					  node.setKeys(keys);
					  node.setValues(values);
					  delete(right,min.getKeys().get(0));
					  }
				  else {
					  if(node==root&&node.getNumOfKeys()==1) {
						  merge(node,r);
						  delete(root,key);
						//  root.getKeys().remove(min-1);
						 // root.getValues().remove(min-1);
					  }
					  else {
					  merge(node,r);
					  delete(node,key);
					 // node.getChildren().get(r).getKeys().remove(min-1);
					 // node.getChildren().get(r).getValues().remove(min-1);
					  }
					  }
			  }
			  break;}
		  else if(keys.get(r).compareTo(key)>0) {
			  IBTreeNode<K, V> child=node.getChildren().get(r);
			  if(child.getNumOfKeys()>min-1) {delete(child, key);}
			  else if(r!=0&&node.getChildren().get(r-1).getNumOfKeys()>min-1) {borrowLeft(node,r-1); delete(child, key);}
			  else if(r!=node.getChildren().size()-1&&node.getChildren().get(r+1).getNumOfKeys()>min-1) {borrowRight(node,r); delete(child, key);}
			  else {
				  if(node==root) {merge(node, r); delete(root,key);}
				  else {merge(node, r); delete(node, key);}
				  }
			  break;
			  }
		  else if(keys.get(r).compareTo(key)<0&&r==node.getNumOfKeys()-1) {
			  r++;
			  IBTreeNode<K, V> child=node.getChildren().get(r);
			  if(child.getNumOfKeys()>min-1) {delete(child, key);}
			  else if(r!=0&&node.getChildren().get(r-1).getNumOfKeys()>min-1) {borrowLeft(node,r-1); delete(child, key);}
			  else if(r!=node.getChildren().size()-1&&node.getChildren().get(r+1).getNumOfKeys()>min-1) {borrowRight(node,r); delete(child, key);}
			  else {r--;
			        if(node==root) {merge(node, r); delete(root,key);}
			        else {merge(node, r); delete(node, key);}
			       }
			  break;
		  }
		}
		
		return true;
	}
	
	
	public void merge(IBTreeNode<K, V> node,int r) {
		IBTreeNode<K, V> left=node.getChildren().get(r);
		IBTreeNode<K, V> right=node.getChildren().get(r+1);
		IBTreeNode<K, V> n=new BTreeNode<K,V>();
		n.getKeys().addAll(left.getKeys());
		n.getKeys().add(node.getKeys().get(r));
		n.getKeys().addAll(right.getKeys());
		n.getValues().addAll(left.getValues());
		n.getValues().add(node.getValues().get(r));
		n.getValues().addAll(right.getValues());
		if(!left.isLeaf()) {n.getChildren().addAll(left.getChildren());  n.setLeaf(false);}
		if(!right.isLeaf()) {n.getChildren().addAll(right.getChildren());}
		if(node==root&&node.getNumOfKeys()==1) {root=n;}
		else{node.getChildren().remove(r+1);
		     node.getChildren().remove(r);
		     node.getKeys().remove(r);
		     node.getValues().remove(r);
		     node.getChildren().add(r, n);}
	}
	
	public void borrowRight(IBTreeNode<K, V> node,int r) {
		IBTreeNode<K, V> left=node.getChildren().get(r);
		IBTreeNode<K, V> right=node.getChildren().get(r+1);
		left.getKeys().add(node.getKeys().get(r));
		left.getValues().add(node.getValues().get(r));
		if(!right.isLeaf()) {left.getChildren().add(right.getChildren().get(0));}
		node.getKeys().remove(r);
		node.getValues().remove(r);
		node.getKeys().add(r, right.getKeys().get(0));
		node.getValues().add(r,right.getValues().get(0));
		right.getKeys().remove(0);
		right.getValues().remove(0);
		if(!right.isLeaf()) {right.getChildren().remove(0);}
	}
	
	public void borrowLeft(IBTreeNode<K, V> node,int r) {
		IBTreeNode<K, V> left=node.getChildren().get(r);
		IBTreeNode<K, V> right=node.getChildren().get(r+1);
		right.getKeys().add(0,node.getKeys().get(r));
		right.getValues().add(0,node.getValues().get(r));
		if(!left.isLeaf()) {right.getChildren().add(0,left.getChildren().get(left.getNumOfKeys()));}
		node.getKeys().remove(r);
		node.getValues().remove(r);
		node.getKeys().add(r, left.getKeys().get(left.getNumOfKeys()-1));
		node.getValues().add(r,left.getValues().get(left.getNumOfKeys()-1));
		left.getKeys().remove(left.getNumOfKeys()-1);
		left.getValues().remove(left.getNumOfKeys());
		if(!left.isLeaf()) {left.getChildren().remove(left.getNumOfKeys()+1);}
	}
}

