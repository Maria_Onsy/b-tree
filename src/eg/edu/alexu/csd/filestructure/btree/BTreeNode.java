package eg.edu.alexu.csd.filestructure.btree;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

public class BTreeNode<K extends Comparable<K>,V> implements IBTreeNode<K, V> {
  private boolean isLeaf=true;
  private List<K> keys=new ArrayList<K>();
  private List<V> values=new ArrayList<V>();
  private List<IBTreeNode<K, V>> children=new ArrayList<IBTreeNode<K,V>>();
	@Override
	public int getNumOfKeys() {
		return keys.size();
	}

	@Override
	public void setNumOfKeys(int numOfKeys) {
		while(keys.size()>numOfKeys) {
			keys.remove(keys.size()-1);
			values.remove(values.size()-1);
		}
		if(!isLeaf) {
			while(children.size()>numOfKeys) {
				children.remove(children.size()-1);
			}
		}
	}

	@Override
	public boolean isLeaf() {
		return isLeaf;
	}

	@Override
	public void setLeaf(boolean isLeaf) {
		this.isLeaf=isLeaf;
	}

	@Override
	public List<K> getKeys() {
		return keys;
	}

	@Override
	public void setKeys(List<K> keys) {
		if(keys==null) {throw new RuntimeErrorException(null);}
	    this.keys.clear();
	    this.keys.addAll(keys);
	}

	@Override
	public List<V> getValues() {
		return values;
	}

	@Override
	public void setValues(List<V> values) {
		if(values==null) {throw new RuntimeErrorException(null);}
	    this.values.clear();
	    this.values.addAll(values);
	}

	@Override
	public List<IBTreeNode<K, V>> getChildren() {
		return children;
	}

	@Override
	public void setChildren(List<IBTreeNode<K, V>> children) {
		if(children==null) {throw new RuntimeErrorException(null);}
	    this.children.clear();
	    this.children.addAll(children);
	}

}
