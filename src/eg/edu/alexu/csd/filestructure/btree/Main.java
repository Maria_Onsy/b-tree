package eg.edu.alexu.csd.filestructure.btree;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.management.RuntimeErrorException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.runner.manipulation.Sortable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
public class Main {

	public static void main(String[] args) {
		SearchEngine engine=new SearchEngine(100);
		engine.indexDirectory("res");
		System.out.println("search:");
		Scanner scan=new Scanner(System.in);
		String sentence=scan.nextLine();
		List<ISearchResult> result=new ArrayList<ISearchResult>();
		result=engine.searchByMultipleWordWithRanking(sentence);
		if(result==null) {
			System.out.println("No results found.");
			return;
		}
		sort(result);
		String[][] doc=getDoc(result);
		printDoc(doc);
		
	}
	public static String[][] getDoc(List<ISearchResult> result) {
		String[][] documents=new String[result.size()][2];
		File temp=new File("res");
        if(temp.exists()) {
        	File files[]=temp.listFiles();
        	if(files==null) {throw new RuntimeErrorException(null);}
        	ArrayList<File> fileslist=new ArrayList<File>();
        	for(int i=0;i<files.length;i++) {
    			if(files[i].isDirectory()) {
    				File temp2=new File("res//"+files[i].getName());
    		        if(temp.exists()) {
    		        	File files2[]=temp2.listFiles();
    		        	if(files==null) {throw new RuntimeErrorException(null);}
    		        	for(int j=0;j<files2.length;j++) {
    		        		fileslist.add(files2[j]);
    		        		}
    		        	continue;
    		        	}
    			}
    			else {fileslist.add(files[i]);}
    			}
    		for(int i=0;i<fileslist.size();i++) {
    			try {
    		    	DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
    				DocumentBuilder db=dbf.newDocumentBuilder();
    				Document doc=db.parse(fileslist.get(i));
    				doc.getDocumentElement().normalize();
    				NodeList nodeList=doc.getElementsByTagName("doc");
    				for(int k=0;k<nodeList.getLength();k++) {
    					Node docm=nodeList.item(k);
    					NamedNodeMap nodeMap=docm.getAttributes();
    					String id=nodeMap.getNamedItem("id").getNodeValue();
    				for(int j=0;j<result.size();j++) {
    					if(documents[j][0]!=null) {continue;}
    					if(id.equals(result.get(j).getId())) {
    					  documents[j][0]=docm.getTextContent();
    					  documents[j][1]=nodeMap.getNamedItem("title").getNodeValue();
    					}
    				}}
    			}
    			catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
        }
        else {throw new RuntimeErrorException(null);}
        return documents;
	}
	
	public static void printDoc(String[][] doc) {
		for(int i=0;i<doc.length;i++) {
			System.out.println("Title:"+doc[i][1]);
			System.out.println(doc[i][0]);
			System.out.println("\n");
		}
	}
	
	public static void sort(List<ISearchResult> result) {
		boolean sorted=false;
		ISearchResult temp;
		while(!sorted) {
			sorted=true;
			for(int i=0;i<result.size()-1;i++) {
				if(result.get(i).getRank()<result.get(i+1).getRank()) {
					temp=result.get(i);
					result.remove(i);
					result.add(i, result.get(i));
					result.remove(i+1);
					result.add(i+1,temp );
					sorted=false;
				}
			}
		}
	} 

}
